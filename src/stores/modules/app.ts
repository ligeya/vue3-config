import { store } from '@/stores'
export const useAppStore = defineStore(
  'app',
  () => {
    const status = ref(666)

    function changeStatus(num) {
      status.value = num
    }

    return {
      status,
      changeStatus
    }
  },
  {
    persist: {
      key: 'my-custom-key'
    }
  }
)

// 手动提供给 useStore() 函数 pinia 实例
export function useAppStoreHook() {
  return useAppStore(store)
}
