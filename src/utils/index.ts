import { useAppStoreHook } from '@/stores/modules/app'

const appStore = useAppStoreHook()
export function getStore() {
  ElMessage(appStore.status.toString())
}

/**
 * 设置Style属性
 *
 * @param propName
 * @param value
 */
export function setStyleProperty(propName: string, value: string) {
  document.documentElement.style.setProperty(propName, value)
}
