import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { setupStore } from '@/stores'
import { setupDirective } from '@/directive'

import 'element-plus/theme-chalk/dark/css-vars.css'
import '@/styles/index.scss'
import 'virtual:uno.css'
import 'virtual:svg-icons-register'

import { getStore } from '@/utils'

import Echarts from 'vue-echarts'
getStore()
const app = createApp(App)
app.component('v-chart', Echarts)

// 全局注册 状态管理(store)
setupStore(app)
// 全局注册 自定义指令(directive)
setupDirective(app)

app.use(router).mount('#app')
