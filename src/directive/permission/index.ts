import type { Directive, DirectiveBinding } from "vue";
export const hasPerm:Directive = {
  mounted(el: HTMLElement, binding:DirectiveBinding) {
    const {value } = binding
    if(value){
      return true;
    }else{
      el.parentNode && el.parentNode.removeChild(el);
    }
  }
}